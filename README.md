# What was refactored
* First of all project was reimported and get rid of Gradle(In order to leave only Maven). Next, was deleted unnecessary
* directories, update .gitignore and a bit modified pom.xml files. Then was updated a project structure and added incode.config
* with constats classes in main package, also added incode.definitions(where added step definition file), incode.steps
* (where added common steps file) and incode.steps.product(where added products separate steps file just for holding
* steps regarding product feature), incode.utils(where added request builder file). Also simplyfied serenity.conf file and
* added .gitlab-ci.yml for having basic configurations for CI/CD processes.

# Add new tests
* New tests can be easily added to existing feature file
* Steps definitions can be added to the existing definitions file
* And steps itself can be described in the existing steps file

# Getting started
install JDK
* download jdk (sapmachine-11.0.10)
* set jdk path to 'JAVA_HOME' environment variable
* add %JAVA_HOME%\bin to 'Path' environment variable

install Apache Maven
* download maven (3.6.3)
* unzip
* set maven path to 'M2_HOME' environment variable
* add %M2_HOME%\bin to 'Path' environment variable

## How to run tests:
- locally - mvn clean test(Report - target/site/*last_created*.html/)
