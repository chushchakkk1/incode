package incode.utils;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;

public class RequestBuilder {

    public static RequestSpecification requestSpecification() {
        EnvironmentVariables environmentVariables = Injectors.getInjector().getInstance(EnvironmentVariables.class);

        return new RequestSpecBuilder()
                .setBaseUri(EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("baseurl"))
                .setContentType("application/json")
                .build();
    }
}