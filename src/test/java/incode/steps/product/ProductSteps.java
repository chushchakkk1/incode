package incode.steps.product;

import incode.utils.RequestBuilder;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;

import java.util.HashMap;
import java.util.List;

import static incode.config.Constants.*;
import static incode.config.QueryConstants.PRODUCT_BY_NAME;
import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static org.assertj.core.api.Assertions.assertThat;

public class ProductSteps {

    public Response getProductByParam(String product) {
        return SerenityRest
                .given()
                .log()
                .uri()
                .spec(RequestBuilder.requestSpecification())
                .pathParam(PRODUCT, product)
                .get(PRODUCT_BY_NAME);
    }

    public void verityProductPresenceInResponse(String productResponse){
        List<HashMap<String, Object>> products = lastResponse().jsonPath().getList(EMPTY_STRING);
        assertThat(products).anyMatch(product -> product.get(TITLE).toString().contains(productResponse));
    }

}