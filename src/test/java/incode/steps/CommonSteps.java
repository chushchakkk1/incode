package incode.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static net.serenitybdd.rest.SerenityRest.then;
import static org.hamcrest.Matchers.is;

public class CommonSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonSteps.class);

    public void receiveSuccessResponseCodeIs(int responseCode) {
        LOGGER.info("Response Status Code is {}", responseCode);
        then().statusCode(responseCode);
    }

    public void receiveErrorResponseCodeIs(int responseCode) {
        LOGGER.info("Response Status Code is {}", responseCode);
        then().statusCode(responseCode).body("detail.error", is(true));
    }

}
