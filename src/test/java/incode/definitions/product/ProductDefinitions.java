package incode.definitions.product;

import incode.steps.CommonSteps;
import incode.steps.product.ProductSteps;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import static incode.config.Constants.NOT_FOUND_STATUS_CODE;
import static incode.config.Constants.OK_STATUS_CODE;

public class ProductDefinitions {

    @Steps
    public CommonSteps commonSteps;

    @Steps
    public ProductSteps productSteps;

    @When("^User receives product (.*) from endpoint$")
    public void userReceivesProductFromEndpoint(String productName) {
        productSteps.getProductByParam(productName);
    }

    @Then("^User verifies the product (.*) should be present and displayed in response$")
    public void userVerifiesProductPresentAndDisplayed(String productName) {
        commonSteps.receiveSuccessResponseCodeIs(Integer.parseInt(OK_STATUS_CODE));
        productSteps.verityProductPresenceInResponse(productName);
    }

    @Then("User receives Not Found error as response")
    public void userReceivesNotFoundError() {
        commonSteps.receiveErrorResponseCodeIs(Integer.parseInt(NOT_FOUND_STATUS_CODE));
    }
}
