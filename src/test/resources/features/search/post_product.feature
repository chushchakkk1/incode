Feature: Search for the product

  @positiveScenario
  Scenario Outline: Verify existing Products
    When User receives product <product> from endpoint
    Then User verifies the product <product> should be present and displayed in response
    Examples:
      | product |
      | pasta   |
      | cola    |

  @negativeScenario
  Scenario Outline: Verify unexisting Products
    When User receives product <product> from endpoint
    Then User receives Not Found error as response
    Examples:
      | product |
      | car     |
      | mango   |
