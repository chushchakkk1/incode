package incode.config;

public class Constants {

    public static final String PRODUCT = "product";
    public static final String TITLE = "title";
    public static final String EMPTY_STRING = "";
    public static final String OK_STATUS_CODE = "200";
    public static final String NOT_FOUND_STATUS_CODE = "404";
}
